#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# libpeer 1.2.2
# A Python library for Peerbet bots
# Licensed with GNU GPL v3
#
# 5th-17th April 2013 espectalll123 <espectalll123@yahoo.es>
#
# Powered by Peerbet's API, 2012-2013 giantdragon

from urllib2 import urlopen
from json import load as json_load
from urllib import quote_plus as urlize

def json_run(url, data) :
	s = "https://peerbet.org/api.php?" + url + data
	if (debug == True) :
        	print "\nConnecting to " + s
        r = urlopen(s)
	o = json_load(r)
	if (debug == True) :
        	print r.info()
        	print "Returning code is " + str(r.getcode())
        	print "Returning data is " + str(o)
        	print ""
        return o

def start() :
	global username
	global password
	global email
	global debug

	debug = True
	
	details = open('details.txt')
	loop = 1
	for line in details:
		if (loop == 1) :
			username = urlize(line.split('\n')[0])
		if (loop == 2) :
			password = urlize(line.split('\n')[0])
		if (loop == 3) :
			email = line.split('\n')[0]
		if (line.split('\n')[0] == "nodebug") :
			debug = False
		loop += 1
	
	if (debug == True) :
		print "\nWelcome to libpeer!"
		print "Now you can continue using signup() or login()\n"

def signup() :
	url = "method=signup"
	data = '&username=' + username + '&email=' + email + '&password=' + password
	
	o = json_run(url, data)
	return o

def login() :
	url = "method=login"
	data = '&username=' + username + '&password=' + password
	
	o = json_run(url, data)

	global key
	key = o['key']

	return o

def echo(message) :
	url = "method=postchatmessage"
	data = '&message=' + urlize(message) + '&bot=' + '1' + "&key=" + key
	
	o = json_run(url, data)
	return o

def read(cid) :
	url = "method=getchatmessages"
	data = "&key=" + key + '&lastmessageid=' + cid
	
	o = json_run(url, data)
	return o

def read50() :
	url = "method=getchatmessages"
	data = "&key=" + key

	o = json_run(url, data)
	return o

def lmid() :
	l50 = read50()
	lid = l50['message_list'][len(l50['message_list'])-1]['message_id']
	return lid

def lmid_list(amsg) :
	lid = "-1"
	if (len(amsg['message_list']) != 0) :
		lid = amsg['message_list'][len(amsg['message_list'])-1]['message_id']
	return lid

def balance() :
	url = "method=getuserinfo"
	data = "&stats=1" + "&key=" + key

	o = json_run(url, data)
	return o

def address() :
	url = "method=getdepositaddress"
	data = "&key=" + key

	o = json_run(url, data)
	return o

def raffles_list() :
	url = "method=getactiveraffles"
	data = ""

	o = json_run(url, data)
	return o

def raffle(rid) :
	url = "method=getraffleinfo"
	data = "&raffle=" + rid + "&key=" + key

	o = json_run(url, data)
	return o

def my_raffles() :
	url = "method=getmyraffles"
	data = "&key=" + key

	o = json_run(url, data)
	return o

def my_raffles_count() :
	url = "method=getmyrafflecount"
	data = "&key=" + key

	o = json_run(url, data)
	return o

def new(t, p, i, e) :
	url = "method=createraffle"
	data = '&tickets=' + t + '&price=' + p + '&instant=' + i + '&expire=' + e + "&key=" + key
	
	o = json_run(url, data)
	return o

def pnew(t, p, i, pssw, e) :
	url = "method=createraffle"
	data = '&tickets=' + t + '&price=' + p + '&instant=' + i + '&password=' + urlize(pssw) + '&expire=' + e + "&key=" + key
	
	o = json_run(url, data)
	return o

def buy(rid, t) :
	url = "method=buytickets"
	data = "&raffle=" + rid + "&tickets=" + t + "&key=" + key

	o = json_run(url, data)
	return o

def refund(rid) :
	url = "method=refund"
	data = "&raffle=" + rid + "&key=" + key

	o = json_run(url, data)
	return o

def authorize(rid, pssw) :
	url = "method=authorize"
	data = "&raffle=" + rid + '&password=' + urlize(pssw) + "&key=" + key

	o = json_run(url, data)
	return o

def transfer(rec, am) :
	url = "method=transfer"
	data = "&recipient=" + urlize(rec) + '&amount=' + am + "&key=" + key

	o = json_run(url, data)
	return o
